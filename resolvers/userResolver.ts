import { Query, Resolver, Mutation, Arg } from 'type-graphql'
import { User, UserInput } from '../schemas/User'

@Resolver((of) => User)
export class UserResolver {
  private users: User[] = [
    { "id": 1, "userName": "Chuck", "role": "administrator", "active": true },
    { "id": 2, "userName": "Dennis R.", "role": "developer", "active": true },
    { "id": 3, "userName": "Ane", "role": "visitor", "active": true }
  ]

  @Query((returns) => [User], { nullable: true })
  async getUsers(): Promise<User[]> { return await this.users }

  @Mutation((returns) => User)
  async addUser( @Arg('userInput') { userName, role, active }: UserInput): Promise<User> {
    const user = {
      id: this.users.slice(-1)[0].id +1,
      userName,
      role,
      active
    }

    await this.users.push(user)
    return user
  }
}