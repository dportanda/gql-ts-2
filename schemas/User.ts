import { Field, ObjectType, InputType } from 'type-graphql';

@ObjectType()
export class User {
  @Field()
  id: number;

  @Field()
  userName: String;

  @Field()
  role: String;

  @Field()
  active: Boolean;
}


@InputType()
export class UserInput implements Partial<User> {
  @Field()
  userName: String;

  @Field()
  role: String;

  @Field()
  active: Boolean;
}